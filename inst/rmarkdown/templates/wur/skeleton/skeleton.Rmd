---
title: A minimal example
subtitle: With a subtitle
author: Some Body
date: \today
institute: Wageningen Marine Research (WMR), IJmuiden.
output:
  wmrkdown::wur
fontsize: 11pt
---

```{r,setup, include=FALSE}
library(knitr)
opts_chunk$set(cache=TRUE, echo=FALSE, fig.align="center", out.width="70%")
```

# First Slide

Hello, world!

# Second Slide

## Bulleted Lists

- Element A
- Element B
    - B.1
    - B.2
- Element C    


# Math

\begin{equation*}
    e = \lim_{n\to \infty} \left(1 + \frac{1}{n}\right)^n
\end{equation*}

# R Figure Example

The following code generates the plot on the next slide (taken from
`help(bxp)` and modified slightly):

```{r pressureCode, eval=FALSE}
library(stats)
set.seed(753)
bx.p <- boxplot(split(rt(100, 4),
                      gl(5, 20)), plot=FALSE)
bxp(bx.p, notch = FALSE, boxfill = "lightblue",
    frame = FALSE, outl = TRUE,
    main = "Example from help(bxp)")
```  

# R Figure Example
  
```{r pressureFig, echo=FALSE}
library(stats)
set.seed(753)
bx.p <- boxplot(split(rt(100, 4),
                      gl(5, 20)), plot=FALSE)
bxp(bx.p, notch = FALSE, boxfill = "lightblue",
    frame = FALSE, outl = TRUE,
    main = "Example from help(bxp)")
```  

# R Table Example
  
A simple `knitr::kable` example:  

```{r kableEx}
knitr::kable(mtcars[1:5, 1:8],
             caption="(Parts of) the mtcars dataset")
```

## Resources

- See the [RMarkdown repository](https://github.com/rstudio/rmarkdown) for more on RMarkdown
